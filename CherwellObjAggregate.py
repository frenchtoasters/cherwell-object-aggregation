from __future__ import print_function
from CherwellAPI import CherwellClient
from CherwellAPI import Filter
from CherwellAPI import BusinessObjects
import CherwellAPI
import json

# Test libraries
from CherwellTestAPI import CherwellTestClient
from CherwellTestAPI import TestBusinessObject


class ObjAggregateRule(object):
    """
    ObjAggregateRule object format
    :standard rule - min:
    {
        "comparison": "contains"
    }
    :standard rule - full:
    {
        "comparison": "contains",
        "threshold": 2
    }
    """

    def __init__(self, rule: dict):
        """
        Inialize a rule
        :param rule: dict following format outlined above
        """
        self.raw = {}
        self.threshold = None
        self.comparison = None
        if isinstance(rule, dict):
            self.raw = rule
            self.threshold = rule['threshold']
            if "comparison" in rule.keys():
                self.comparison = rule['comparison']
            else:
                self.comparison = None


class ObjAggregateRules(object):
    """
    ObjAggregateRules object format
    :standard rules:
    {
        "name": {"comparison": "contains", "threshold": 2},
        "tags": {"comparison": "eq", "threshold": 3},
        "site": {"threshold": 5}
    }
    """

    def __init__(self, rules_set: dict):
        """
        Initalize the rules object
        :param rules_set: dict of rules following standard outlined above
        """
        if rules_set is None:
            self.keys = []
            self.rules = {}
        else:
            self.rules = {}
            self.keys = []
            for rule in rules_set:
                if "threshold" in rules_set[rule].keys():
                    self.keys.append(rule)
                    self.rules[rule] = ObjAggregateRule(rules_set[rule])
                else:
                    print("invalid rule: {}".format(rule))
                    continue

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, item):
        return self.rules[self.keys[item]]

    def __setitem__(self, key, value):
        self.rules[key] = value


class ObjAggregateCache(object):
    def __init__(self, uri, api_key):
        """
        Initalizes the cache used for the Aggregation object
        :param uri: Cherwell instance
        :param api_key: API key for the Cherwell user
        """
        self.cache = {
            'uri': uri,
            'api_key': api_key,
            'results': {},
            'parent_objs': [],
            'child_objs': []
        }

    def contents(self) -> dict:
        """
        Returns the full cache
        :return: dict()
        """
        return self.cache

    def contentsToJson(self) -> str:
        """
        Returns the json value of the cache
        :return: str
        """
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def get_results(self) -> list:
        """
        Returns the uri for the cached cherwell instance
        :return: dict()
        """

        return self.cache['results']

    def set_results(self, results: dict):
        """
        Updates the cache with the content of results dict
        :param results:
        {
            rule_name: {
                "count": int(), // Number of records found
                "status": str(), // Status of rule
                "objects": list(CherwellClient.BusinessObject), // List of child objects
                "name": str() // UUID used to match to Parent
            },
            ...
        }
        """
        self.cache['results'].update(results)

    def set_parent_objs(self, parent: CherwellAPI.BusinessObjects.BusinessObject):
        """
        Updates the cache with the content of the of parent object created
        :param parent:
        [
            CherwellAPI.BusinessObject,
            ...
        ]
        """
        if parent.busObRecId not in zip(d.busObRecId for d in self.cache['parent_objs']):
            if isinstance(parent, CherwellAPI.BusinessObjects.BusinessObject) or \
                    isinstance(parent, TestBusinessObject.TestBusinessObject):
                self.cache['parent_objs'].append(parent)
            else:
                print("wrong type for parent object")

    def set_child_objs(self, child: CherwellAPI.BusinessObjects.BusinessObject):
        """
        Updates teh cache with the content of the child objects updated
        :param child:
        [
            CherwellAPI.BusinessObject,
            ...
        ]
        """
        self.cache['child_objs'].append(child)


class CherwellObjAggregate(object):
    def __init__(self, uri: str, username: str, password: str, api_key: str, cache: ObjAggregateCache = None,
                 client: CherwellClient.Connection = None, client_test: CherwellTestClient.Connection = None,
                 offline: bool = False, rules: dict = None):
        """
        Initalize the Aggregate object and cache
        :param uri: Url of cherwell instance
        :param username: User name
        :param password: Password
        :param api_key: API key for user
        :param cache: dict() containing,
        :param client:
        """
        self.child = ""
        self.parent = ""
        self.offline = offline

        # Create AggregateCache if does not already exist
        if isinstance(cache, ObjAggregateCache):
            self.cache = cache
        else:
            self.cache = ObjAggregateCache(uri, api_key)

        # Load Rules set if does not already exist
        if isinstance(rules, ObjAggregateRules):
            self.rules = rules
        else:
            self.rules = ObjAggregateRules(rules)

        # Create Connection if does not already exist
        if isinstance(client, CherwellClient.Connection):
            self.client = client
        else:
            self.client = CherwellClient.Connection(uri, api_key, username, password)

        # If offline test set to True, override client to use test Client, Combs style
        if offline:
            if isinstance(client_test, CherwellTestClient.Connection):
                self.client = client_test
            else:
                self.client = CherwellTestClient.Connection(uri, api_key, username, password)

    def get_rules(self) -> ObjAggregateRules:
        """
        Returns the rules for the cached CherwellObjAggregate instance
        """
        return self.rules

    def aggregate_objects(self, child: str, parent: str, child_status: str, match_field: str):
        """
        Aggregates a child business object into a parent business object
        :param child: Name of the child business object
        :param parent: Name of the parent business object
        :param child_status: Name of the status for which to search for orphaned children and match to their parents
        :param match_field: Unique field which can be used to match a child to its parent
        :return: List of parent objects created
        """
        testing = 1
        alert_status = ["firing", "triggered"]
        filters = Filter.AdHocFilter(child)
        filters.add_search_fields("Status", "eq", child_status)

        # Add all rule fields
        for rule in self.rules.keys:
            filters.add_fields(rule)

        records, business_objects = self.client.get_business_records(filters)

        if records > 0:
            # Here is where we start writing to cache
            self._process_rules(business_objects)

        results = self.cache.contents()['results']
        for result in results:
            if results[result]['status'] in alert_status:
                filters = Filter.SimpleFilter(parent)
                filters.add_search_fields(match_field, "eq", results[result]['name'])
                filters.add_search_fields("Status", "eq", results[result]['status'])
                records, business_objects = self.client.get_business_objects(filters)
                if records > 0:
                    # Matching parent found
                    # Preform search for all child objects
                    filters = Filter.SimpleFilter(child)
                    for obj in results[result]['objects']:
                        filters.add_search_fields("busObRecId", "eq", obj['busObRecId'])
                    records, child_objs = self.client.get_business_objects(filters)

                    # If any child objects are found
                    if records > 0:
                        for child_obj in child_objs:
                            child_obj.Incident = business_objects[0].busObRecId
                            if not any(d.busObRecId == business_objects[0].busObRecId
                                       for d in self.cache.contents()['parent_objs']):
                                self.cache.set_parent_objs(business_objects[0])
                            self.cache.set_child_objs(child_obj)
                        self.client.save_business_objects(child_objs)
                else:
                    # There was no existing parent object so we need to create one
                    parent_template = self.client.get_business_object_template(parent)
                    self.client.set_business_object_field_value_in_template(
                        parent_template,
                        match_field,
                        results[result]['name']
                    )
                    self.client.set_business_object_field_value_in_template(
                        parent_template,
                        "Status",
                        results[result]['status']
                    )

                    # Save the new parent
                    new_par_public_id, new_par_rec_id, business_object_id = \
                        self.client.create_business_object(parent_template, parent)

                    # Get new parent object
                    filters = Filter.SimpleFilter(parent)
                    filters.add_search_fields("busObRecId", "eq", business_object_id)
                    records, parent_objs = self.client.get_business_objects(filters)

                    # Preform search for all child objects
                    filters = Filter.SimpleFilter(child)
                    for obj in results[result]['objects']:
                        filters.add_search_fields("busObRecId", "eq", obj['busObRecId'])
                    records, business_objects = self.client.get_business_objects(filters)

                    # If child objects found set incident field
                    if records > 0:
                        for child_obj in business_objects:
                            child_obj.Incident = new_par_rec_id
                            self.cache.set_child_objs(child_obj)
                        for parent_obj in parent_objs:
                            self.cache.set_parent_objs(parent_obj)
                        self.client.save_business_objects(business_objects)
            else:
                # No alerts met their threshold, resolve any parent objects found
                # Note: When testing there is always a parent found so for each rule there will be a `fixed` parent_obj
                filters = Filter.SimpleFilter(parent)
                filters.add_search_fields(match_field, "eq", results[result]['name'])
                records, business_objects = self.client.get_business_objects(filters)
                if records > 0:
                    for parent_obj in business_objects:
                        parent_obj.Status = "fixed"
                        self.cache.set_parent_objs(parent_obj)
                    self.client.save_business_objects(business_objects)

            # Test
            # Run a test to ensure we can create a parent
            if self.offline and testing:
                # Test testing flag so only run test once
                testing = 0
                parent_template = self.client.get_business_object_template(parent)
                self.client.set_business_object_field_value_in_template(parent_template, match_field,
                                                                        results[result]['name'])
                self.client.set_business_object_field_value_in_template(parent_template, "Status",
                                                                        results[result]['status'])

                # Save the new parent
                new_par_public_id, new_par_rec_id, business_object_id = self.client.create_business_object(
                    parent_template, parent)

                # Preform search for all child objects
                filters = Filter.SimpleFilter(child)
                for obj in results[result]['objects']:
                    filters.add_search_fields("busObRecId", "eq", obj['busObRecId'])
                records, business_objects = self.client.get_business_objects(filters)

                # If child objects found set incident field
                if records > 0:
                    for child_obj in business_objects:
                        child_obj.Incident = new_par_rec_id
                        if not any(d.busObRecId == new_par_rec_id
                                   for d in self.cache.contents()['parent_objs']):
                            self.cache.set_parent_objs(child_obj)
                        self.cache.set_child_objs(child_obj)
                    self.client.save_business_objects(business_objects)

    def _process_rules(self, objs):
        """
        Query based on loaded rules and store results in cache.
        :param objs: Business Objects that are passed for processing
        :return: None
        """
        filters = Filter.AdHocFilter("Alerts")
        results = {}
        for obj in objs:
            for field in obj.get('fields'):
                if field['name'] in self.rules.keys:
                    """
                    Here is where we do the logic of the rule types accepted types:
                    - lt, less than
                    - gt, greater than
                    - eq, equal
                    - contains, like comparison // default if true
                    
                    Here if there is no comparison operator we add the filed as a returned field in the business record
                    for futher evalutation in a TODO. 
                    Default is to add the field name and the comparison to the value of the filed for each object:
                    Filter.AdHocFilter{
                        "obj[i]['fields'][j]['value']", // Incident
                        "rule[i]['comparison']", // eq
                        "obj[i]['fileds'][j]['value']" // 12534
                    }
                    """
                    if not self.rules.rules.get(field['name']).comparison:
                        filters.add_fields(field['name'])
                    else:
                        filters.add_search_fields(
                            field['name'],
                            self.rules.rules.get(field['name']).comparison,
                            field['value']
                        )
                records, business_objects = self.client.get_business_records(filters)

                if field['name'] in self.rules.keys:
                    results[field['name']] = {
                        "count": 0,
                        "objects": [],
                        "status": ""
                    }
                    results[field['name']]['count'] = records
                    if records >= self.rules.rules.get(field['name']).threshold:
                        results[field['name']]['status'] = "firing"
                        results[field['name']]['objects'] = []
                        for business_object in business_objects:
                            results[field['name']]['objects'].append(business_object)
                    else:
                        results[field['name']]['status'] = "resolved"
                        results[field['name']]['objects'] = []
                        for business_object in business_objects:
                            results[field['name']]['objects'].append(business_object)
                    results[field['name']]['name'] = field['value']
        self.cache.set_results(results)
