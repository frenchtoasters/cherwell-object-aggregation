from CherwellObjAggregate import CherwellObjAggregate

test_rules = {
            "Incident": {"comparison": "contains", "threshold": 2},
            "Parent": {"comparison": "eq", "threshold": 5},
            "status": {"threshold": 10}
        }

test_agg = CherwellObjAggregate(uri="fake.test.com", username="test", password="mctest",
                                api_key="mctestykey", offline=True, rules=test_rules)

test_agg.aggregate_objects(child="93d5c17090fa0a4981c57b459eb051c83f5234333b", parent="93d5c17090fa0a4981c57b459eb051c83f5234333b",
                           child_status="firing", match_field="")

with open('cache.json', 'w') as f:
    f.write(test_agg.cache.contentsToJson())
print("Complete")