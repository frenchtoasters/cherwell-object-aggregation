import gitlab
import os


class ToDoBot(object):
    def __init__(self, uri: str, token_type: str, token: str, noted: list = None):
        self.issues = None
        self.opened_titles = None
        self.noted = None
        if isinstance(noted, list):
            self.noted = noted
        else:
            self.noted = self._load_files(os.getcwd())

        if token_type == 'private' or token_type == 'personal':
            self.gl = gitlab.Gitlab(uri, private_token=token)
            self.project = self.gl.projects.get(os.environ['CI_PROJECT_ID'])
        elif token_type == 'oauth':
            self.gl = gitlab.Gitlab(uri, oauth_token=token)
            self.project = self.gl.projects.get(os.environ['CI_PROJECT_ID'])
        elif token_type == 'job':
            self.gl = gitlab.Gitlab(uri, job_token=token)
            self.project = self.gl.projects.get(os.environ['CI_PROJECT_ID'])
        else:
            self.gl = gitlab.Gitlab(uri)
            self.project = self.gl.projects.get(os.environ['CI_PROJECT_ID'])

    def _load_files(self, dir_path: str) -> list:
        fulllist = []
        for (dirpath, dirnames, filenames) in os.walk(dir_path):
            fulllist += [os.path.join(dirpath, file) for file in filenames]

        check = []
        exclude = ["venv", ".git", ".idea", ".md", ".json", "__pycache__"]
        for subdir in fulllist:
            if any(exc in subdir for exc in exclude):
                continue
            else:
                check.append(subdir)

        noted = []
        for file in check:
            with open(file, 'r') as f:
                data = f.readlines()
            for i in range(len(data)):
                if "TODO" in data[i] and "NOTES" in data[i + 1]:
                    title = data[i].strip().strip('# ')
                    # TODO::logic to capture all notes lines
                    # NOTES::if a notes comment is multiple lines, which is likely
                    # :: we should capture all the lines after with `# :`
                    j = 2
                    description = data[i + 1].strip().strip('# ')
                    while "# :" in data[i + j]:
                        description += " " + (data[i + j].strip().strip('# :'))
                        j += 1
                    noted.append({"name": title, "description": description})
        print(noted)
        return noted

    def set_task(self, task: str):
        if task == 'issues':
            self.issues = self.project.issues.list(state='opened')
            self.opened_titles = []
            for found in self.issues:
                self.opened_titles.append(found.title)

    def get_noted(self) -> list:
        return self.noted

bot = ToDoBot('https://gitlab.com', "personal", os.environ['todo_bot_token'])
bot.set_task("issues")

for noted in bot.get_noted():
    if noted['name'] not in bot.opened_titles:
        issue = bot.project.issues.create({'title': noted['name'], 'description': noted['description']})
        issue.labels = ['todobot']
        issue.save()
        print("Creating issue: {}".format(issue.title))
