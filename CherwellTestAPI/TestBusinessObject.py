#####################################################################################
# Class that defines a Business Object - to be used to get new or existing business
# objects
#####################################################################################
import json
import os
from flask import Response

class TestBusinessObject:
    """
    The BusinessObject class serves to simulate a Cherwell BusinessObject and removes
    the need to worry about Cherwell Business Object ID's and Field ID's. Its all taken care of automatically
    in this class.
    ----------
    ***NOTE***
    ----------
        This class should not be instantiated directly, but should be instantiated through the methods of the
        CherwellClient.Connection class
    Parameters
    ----------
    obj : dict()
        The type_name refers to the Cherwell Business Object internalname that you wish to instantiate
    """

    def __init__(self, type_name, get_header_function, save_uri, delete_uri, busObId, busObRecId="", busObPublicId=""):

        self.type_name = type_name
        self.busObId = busObId
        self.busObPublicId = busObPublicId
        self.persist = True
        self.busobj_fields = []
        self.Status = ""
        self.get_header = get_header_function
        self.save_uri = save_uri
        self.delete_uri = delete_uri
        self.busObRecId = busObRecId
        self.has_Error = False
        self.error_Message = ""

    def __setattr__(self, key, value):
        """ Populates the attributes of this object instance, using the fields from the Business Object"""

        # Change the instance attribute
        self.__dict__[key] = value

    # Load the business object attributes from a passed in template
    def load(self, template):
        """ Loads the fields into this BusinessObject instance from a passed in template"""

        # Loop though the fields in the template and add them to the fields array
        for field in template["fields"]:
            self.busobj_fields.append(
                {"name": field["name"],
                 "displayName": field["displayName"],
                 "value": field["value"],
                 "html": field["html"],
                 "dirty": field["dirty"],
                 "fieldId": field["fieldId"]}
            )

            # Add the field names and values to instance attributes
            self.__dict__[field['name']] = field['value']

    def Save(self):

        """ Saves this BusinessObject back to Cherwell """

        # Loop through the fields originally loaded from the template
        # and find the instance attributes that have changed - setting them to dirty
        for field in self.busobj_fields:

            # Loop through the object attributes
            for instance_name in self.__dict__:

                if instance_name == field["name"] and self.__dict__[instance_name] != field["value"]:
                    # Instance attribute has been changed from original - set the dirty flag
                    field["dirty"] = True
                    field["value"] = self.__dict__[instance_name]

        # Build the payload for the save
        save_payload = {"Persist": True, "busObID": self.busObId, "busObPublicId": self.busObPublicId,
                        "busObRecId": self.busObRecId, "fields": self.busobj_fields}

        # Get save record result from test data
        result_save = self.__get_save_record(self.save_uri, json_data=save_payload, headers=self.get_header)

        # Set the business object error states
        self.has_Error = result_save["hasError"]
        self.error_Message = result_save["errorMessage"]

        if result_save['status_code'] == 200:

            # Successfully saved the record
            self.busObPublicId = str(result_save["busObPublicId"])
            self.busObRecId = str(result_save["busObRecId"])
        else:
            # There was a problem with the API call, generate an exception
            raise Exception("Error saving business object '{}'. HTTP:{} '{}'".format(
                self.type_name,
                result_save['status_code'],
                self.error_Message))

        self.busObPublicId = str(save_payload["busObPublicId"])
        self.busObRecId = str(save_payload["busObRecId"])

    def __get_save_record(self, uri: str, json_data: dict, headers: str) -> json:
        if json_data.keys():
            if isinstance(headers, str):
                if isinstance(uri, str):
                    with open(os.path.dirname(os.path.realpath(__file__))+"\\responses\\test_get_save_record_response.json", 'r') as json_file:
                        return json.load(json_file)
