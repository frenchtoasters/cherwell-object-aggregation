# Cherwell Object Aggregation

This library is to be used to accomplish the following:

* Aggregate all child objects to a single parent object, based on the `match_filed` option
* Evaluate child objects based on rule sets and determine if the rules are `firing` or `resolved`
* Create `parent` objects and link the `child` objects to them 

## Rules

Rule sets are described as dictionaries that should look like the following:

```json
{
   "entityName": {"comparison": "contains", "threshold": 2},
   "entityTags": {"comparison": "eq", "threshold": 3},
   "entitySite": {"threshold": 5}
}
```

### Rule Types

There are two types of rules, ones that either have a `comparison` or _not_, the behaviour is defined as follows:

* If a rule.value has `comparison` defined, the rule.key will be added as a search field to the rules query 
* if a rule.value has _no_ `comparison` defined, the rule.key will be added as a field returned in the rules query 

## Testing

For testing if you have access to a Cherwell instance you can test on your objects there, if like me you do not you can
use the `offline=True` flag when creating your `CherwellObjAggregate` object and the test libraries will be used.

### Example Test

```python
from CherwellObjAggregate import CherwellObjAggregate

test_rules = {
            "Incident": {"comparison": "contains", "threshold": 2},
            "Parent": {"comparison": "eq", "threshold": 5},
            "status": {"threshold": 10}
        }

test_agg = CherwellObjAggregate(uri="fake.test.com", username="test", password="mctest",
                                api_key="mctestykey", offline=True, rules=test_rules)

test_agg.aggregate_objects(child="93d5c17090fa0a4981c57b459eb051c83f5234333b", parent="93d5c17090fa0a4981c57b459eb051c83f5234333b",
                           child_status="firing", match_field="")

print(test_agg.cache.contentsToJson())
print("Complete")
```

#### Testing Notes

* the field `totalRows` controls the count for all fields when testing 